class UnbordingContent {
  String image;
  String title;
  String discription;

  UnbordingContent(
      {required this.image, required this.title, required this.discription});
}

List<UnbordingContent> contents = [
  UnbordingContent(
      title: 'SEND & RECIEVE',
      image: 'assets/Frame.png',
      discription: "Send anything like document, photos,"
          "clothes, urgent medicines with our "
          "traveler community "),
  UnbordingContent(
      title: 'TRAVEL & EARN',
      image: 'assets/frame2.png',
      discription: "Earn extra \$\$ while offering some "
          "space in you bag or in your vehicle "),
];
